class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int m = grid.size();
        if(m == 0) return 0;
        int n = grid[0].size();
        int num = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == '1') {
                    numIslandsHelper(m, n, i, j, grid);
                    num++;
                }
            }
        }
        return num;
    }

    void numIslandsHelper(int m, int n, int i, int j, vector<vector<char>>& grid) {
        if(i < 0 || i >= m || j < 0 || j>=n || grid[i][j] == '0') {
            return;
        }

        grid[i][j] = '0';
        numIslandsHelper(m, n, i+1, j, grid);
        numIslandsHelper(m, n, i-1, j, grid);
        numIslandsHelper(m, n, i, j+1, grid);
        numIslandsHelper(m, n, i, j-1, grid);

    }
};
