class Solution {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> permutations = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
        Arrays.sort(nums);
        findPermutation(nums, used, permutations, new ArrayList<>());
        return permutations;
    }

    private void findPermutation(int[] nums, boolean[] used, List<List<Integer>> permutations, List<Integer> tmp) {
        if(tmp.size() == nums.length) {
            permutations.add(new ArrayList<>(tmp));
            return;
        }

        for(int i = 0; i < nums.length; i++) {
            if(used[i]) {
                continue;
            }
            used[i] = true;
            tmp.add(nums[i]);
            findPermutation(nums, used, permutations, tmp);
            used[i] = false;
            tmp.remove(tmp.size()-1);
            int num = nums[i];
            while(i < nums.length -1 && nums[i+1] == num) {
                i++;
            }
        }
    }
}
