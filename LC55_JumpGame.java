class JumpGame{
    public boolean canJump(int[] nums) {
        if(nums == null) return false;
        int last = nums.length - 1;

        for(int i = nums.length -2; i >= 0; i--) {
            // check if the point can actually reach the last
            if(i + nums[i] >= last) {
                last = i;
            }
        }

        return (last == 0) ? true:false;
    }
}
