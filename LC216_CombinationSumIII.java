class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<>();
        findCombinationSum(k, n, 1, 0, result, new ArrayList<>());
        return result;
    }

    private void findCombinationSum(int k, int n, int start, int sum, List<List<Integer>> result, List<Integer> tmp) {
        if(tmp.size() == k && sum == n) {
            result.add(new ArrayList<>(tmp));
            return;
        }

        if(sum > n) {
            return;
        }

        for(int i = start; i <= 9; i++) {
            tmp.add(i);
            findCombinationSum(k, n, i+1, sum+i, result, tmp);
            tmp.remove(tmp.size()-1);
        }
    }
}
