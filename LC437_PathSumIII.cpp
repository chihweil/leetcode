/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int pathSum(TreeNode* root, int sum) {
        if(root == NULL) {
            return 0;
        }

        return pathSumHelper(root, sum) + pathSum(root->left, sum) + pathSum(root->right, sum);
    }

    int pathSumHelper(TreeNode* root, int sum) {
        if(root == NULL) {
            return 0;
        }

        int left = pathSumHelper(root->left, sum - root->val);
        int right = pathSumHelper(root->right, sum - root->val);

        if(root->val == sum) {
            return 1 + left + right;
        } else {
            return 0 + left + right;
        }
    }
};
