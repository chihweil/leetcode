/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        if(root == NULL) {
            return true;
        }
        stack<TreeNode* > s;
        TreeNode* cur = root;
        TreeNode* pre = NULL;

        while(!s.empty() || cur) {

            while(cur) {
                s.push(cur);
                cur = cur->left;
            }

            TreeNode* node = s.top();
            if(pre && node->val <= pre->val) {
                return false;
            }

            pre = node;
            cur = node->right;
            s.pop();
        }
        return true;
    }
};
