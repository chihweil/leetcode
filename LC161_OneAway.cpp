class Solution {
public:
    bool isOneEditDistance(string s, string t) {
        if(s == t) {
            return false;
        }

        int sLen = s.size();
        int tLen = t.size();
        if(abs(sLen - tLen) > 1) {
            return false;
        }

        int size = min(sLen, tLen);

        for(int i = 0; i < size; i++) {
            if(s[i] != t[i]) {
                if(sLen > tLen) {
                    return s.substr(i+1) == t.substr(i); //remove
                } else if(sLen == tLen) {
                    return (i+1 < size) ? s.substr(i+1) == t.substr(i+1) : true; //replace
                } else {
                    return s.substr(i) == t.substr(i+1); //insert
                }
            }
        }

        return true;
    }
};
