
class Solution {
public:
    int findCircleNum(vector<vector<int>>& M) {
        int m = M.size();
        if(m == 0) return 0;
        int n = M[0].size();
        vector<int> parent(m, 0);
        for(int i = 0; i < m; i++) {
            parent[i] = i;
        }

        int count = m;
        for(int i = 0; i < m; i++) {
            for(int j = i+1; j < n; j++) {
                if(M[i][j] == 1) {
                    int p1 = find(i, parent);
                    int p2 = find(j, parent);
                    if(p1 != p2) {
                        doUnion(p1, p2, parent);
                        count--;
                    }
                }
            }
        }
        return count;
    }

private:
    int find(int x, vector<int>& parent) {
        while(x != parent[x]) {
            parent[x] = parent[parent[x]];
            x = parent[x];
        }
        return x;
    }

    void doUnion(int x, int y, vector<int>& parent) {
        parent[x] = y;
    }
};
