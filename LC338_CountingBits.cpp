vector<int> countBits(int num) {
        vector<int> res;
        res.push_back(0);
        int exp = 1;

        for(int i = 1; i <= num; i++) {
            if(i >= pow(2, exp)) {
                exp++;
            }
            int mod =  pow(2, exp - 1);
            int bit = res[i % mod] + 1;
            res.push_back(bit);
        }

        return res;
    }
