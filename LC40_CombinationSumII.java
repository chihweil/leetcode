class Solution {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(candidates);
        findCombinationSum(candidates, target, 0, 0, result, new ArrayList<>());
        return result;
    }

    private void findCombinationSum(int[] candidates, int target, int start, int sum, List<List<Integer>> result, List<Integer> tmp) {
        if(sum > target) {
            return;
        }

        if(sum == target) {
            result.add(new ArrayList<>(tmp));
            return;
        }

        for(int i = start; i < candidates.length; i++) {
            tmp.add(candidates[i]);
            findCombinationSum(candidates, target, i+1, sum+candidates[i], result, tmp);
            tmp.remove(tmp.size()-1);
            while(i < candidates.length -1 && candidates[i+1] == candidates[i]) {
                i++;
            }
        }
    }
}
