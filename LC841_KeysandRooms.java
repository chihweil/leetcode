class Solution {
    public boolean canVisitAllRooms(List<List<Integer>> rooms) {
        Set visited = new HashSet<>();
        Queue<Integer> q = new LinkedList<>();
        q.add(0);
        while(!q.isEmpty()) {
            int room = q.poll();
            if(visited.contains(room)) {
                continue;
            }
            visited.add(room);
            List<Integer> keys = rooms.get(room);
            for(int i = 0; i < keys.size(); i++) {
                q.add(keys.get(i));
            }
        }

        return (visited.size() == rooms.size()) ? true : false;
    }
}
