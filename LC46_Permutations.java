//Time Complexity - O(n!)
//Space Complexity - O(n)
class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> permutations = new ArrayList<>();
        Set<Integer> used = new HashSet<>();
        findPermutation(nums, used, permutations, new ArrayList<Integer>());
        return permutations;
    }

    private void findPermutation(int[] nums, Set<Integer> used, List<List<Integer>> permutations, List<Integer> perm) {
        if(perm.size() == nums.length){
            permutations.add(new ArrayList<>(perm));
            return;
        }

        for(int i = 0; i<nums.length; i++) {
            if(used.contains(nums[i])) {
                continue;
            }
            used.add(nums[i]);
            perm.add(nums[i]);
            findPermutation(nums, used, permutations, perm);
            used.remove(nums[i]);
            perm.remove(perm.size()-1);
        }
    }


}
