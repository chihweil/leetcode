/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        TreeNode* tmpRoot = root;

        while(tmpRoot != NULL) {
            if(p->val < tmpRoot->val && q->val < tmpRoot->val) {
                tmpRoot = tmpRoot->left;
            } else if(p->val > tmpRoot->val && q->val > tmpRoot->val) {
                tmpRoot = tmpRoot->right;
            } else {
                return tmpRoot;
            }
        }

        return tmpRoot;
    }
};
