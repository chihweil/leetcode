class Solution {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> combination = new ArrayList<>();
        findCombination(n, k, 1, combination, new ArrayList<>());
        return combination;
    }

    private void findCombination(int n, int k, int start, List<List<Integer>> combination, List<Integer> tmp) {
        if(tmp.size() == k) {
            combination.add(new ArrayList<>(tmp));
            return;
        }

        for(int i = start; i <= n; i++) {
            tmp.add(i);
            findCombination(n, k, i+1, combination, tmp);
            tmp.remove(tmp.size()-1);
        }
    }
}
