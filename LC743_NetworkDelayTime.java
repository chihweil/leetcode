class Solution {
    public int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, List<int[]> > graph = new HashMap<>();
        for(int i = 1; i <= N; i++) {
            graph.put(i, new ArrayList<>());
        }
        for(int i = 0; i < times.length; i++) {
            int[] pair = new int[2];
            pair[0] = times[i][1];
            pair[1] = times[i][2];
            int u = times[i][0];
            graph.get(u).add(pair);
        }

        int[] distance = new int[N+1];
        Arrays.fill(distance, Integer.MAX_VALUE);
        Set<Integer> visited = new HashSet<>();
        PriorityQueue<int[]> q = new PriorityQueue<>(new Comparator<int[]>(){
             public int compare(int[] x, int[] y) {
                 return x[1] - y[1];
            }
        });
        q.add(new int[]{K, 0});
        distance[K] = 0;

        while(!q.isEmpty()) {
            int[] node = q.poll();
            visited.add(node[0]);
            List<int[]> neighbors = graph.get(node[0]);
            for(int i = 0; i < neighbors.size(); i++) {
                int target = neighbors.get(i)[0];
                int time = node[1] + neighbors.get(i)[1];
                if(!visited.contains(target)){
                    q.add(new int[] {target, time});
                    distance[target] = Math.min(distance[target], time);
                }
            }
        }
        int dis = 0;
        for(int i = 1; i <= N; i++) {
            if(distance[i] == Integer.MAX_VALUE) {
                return -1;
            }
            dis = Math.max(dis, distance[i]);
        }
        return dis;
    }
}
