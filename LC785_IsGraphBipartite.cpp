class Solution {
public:
    bool isBipartite(vector<vector<int>>& graph) {
        if(graph.size() == 0) return true;
        stack<int> s;
        map<int, int> visited;

        for(int i = 0; i < graph.size(); i++) {
            if(visited.find(i) != visited.end()) {
                continue;
            }

            s.push(i);
            visited[i] = 0;

            while(!s.empty()) {
                int node = s.top();
                s.pop();
                int color = visited[node];
                vector<int> neighbors = graph[node];
                for(int j = 0; j < neighbors.size(); j++) {
                    if(visited.find(neighbors[j]) == visited.end()) {
                        visited[neighbors[j]] = 1-color;
                        s.push(neighbors[j]);
                        continue;
                    }

                    if(visited[neighbors[j]] == color) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
};
