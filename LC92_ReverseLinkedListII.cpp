/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        ListNode* dummy = new ListNode(0);
        dummy->next = head;
        ListNode* pre = dummy;
        int count = 1;
        while(count < m) {
            pre = pre->next;
            count++;
        }
        ListNode* cur = pre->next;
        ListNode* next;
        count = 0;
        while(count < n-m) {
            next = cur->next;
            cur->next = next->next;
            next->next = pre->next;
            pre->next = next;
            count++;
        }
        return dummy->next;
    }
};
