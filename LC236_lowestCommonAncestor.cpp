TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    stack<TreeNode* > stk;
    map<TreeNode*, TreeNode* > parent;

    if(p == root || q == root) {
        return root;
    }

    stk.push(root);

    //BFS
    while(parent.find(p) == parent.end() || parent.find(q) == parent.end()) {
        TreeNode* node = stk.top();
        stk.pop();

        if(node->left != NULL) {
            stk.push(node->left);
            parent[node->left] = node;
        }

        if(node->right != NULL) {
            stk.push(node->right);
            parent[node->right] = node;
        }
    }

    set<TreeNode*> ancestors;
    ancestors.insert(p);

    while(p != NULL) {
        ancestors.insert(parent[p]);
        p = parent[p];
    }

    while(q != NULL) {
        if(ancestors.find(q) != ancestors.end()) {
            return q;
        }
        q = parent[q];
    }

    return root;
}
