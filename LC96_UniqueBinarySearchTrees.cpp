class Solution {
public:
    int numTrees(int n) {
        int dp[n+1] = {0};
        dp[0] = 1;
        dp[1] = 1;

        for(int i = 2; i <= n; i++) {
            for(int j = 1; j <= i; j++) {
                dp[i] += dp[j-1] * dp[i-j];
            }
        }
        return dp[n];
    }

    //recursive solution
    int numTreesHelper(int l, int r) {
        if(l >= r) {
            return 1;
        }
        int total = 0;
        for(int i = l; i <= r; i++) {
            total += numTreesHelper(l, i-1) * numTreesHelper(i+1, r);
        }

        return total;
    }
};
