class Solution {
public:
    string reverseStr(string s, int k) {
        for(int i = 0; i < s.size(); i+= 2*k) {
            reverse(s, i, k);
        }
        return s;
    }

    void reverse(string& s, int start, int k) {
        int end = (start+k) > s.size() ? s.size() -1 : start+k-1;
        while(start < end) {
            swap(s[start], s[end]);
            start++;
            end--;
        }
    }
};
