class Solution {
    public int combinationSum4(int[] nums, int target) {
        Arrays.sort(nums);
        int count = findCombinationSum(nums, target, 0, new HashMap<>());
        return count;
    }

    private int findCombinationSum(int[] nums, int target, int sum, Map<Integer, Integer> map) {
        if(sum == target) {
            return 1;
        }

        if(sum > target) {
            return 0;
        }

        if(map.containsKey(sum)) {
            return map.get(sum);
        }
        int count = 0;
        for(int i = 0; i < nums.length; i++) {
            count += findCombinationSum(nums, target, sum+nums[i], map);
        }

        map.put(sum, count);

        return count;
    }
}
