/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode* head) {
        if(head == NULL) {
            return;
        }
        ListNode* midNode = findMidNode(head);
        ListNode* list1 = head;
        ListNode* list2 = reverse(midNode->next);
        midNode->next = NULL;
        while(list2 != NULL) {
            ListNode* next1 = list1->next;
            ListNode* next2 = list2->next;
            list1->next = list2;
            list2->next = next1;
            list1 = next1;
            list2 = next2;
        }
    }

    ListNode* findMidNode(ListNode* head) {
        ListNode* slow = head;
        ListNode* fast = head;
        while(fast != NULL && fast->next != NULL) {
            slow = slow->next;
            fast = fast->next->next;
        }

        return slow;
    }

    ListNode* reverse(ListNode* head) {
        ListNode* pre = NULL;
        ListNode* cur = head;
        ListNode* next;
        while(cur != NULL) {
            next = cur->next;
            cur->next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }
};
