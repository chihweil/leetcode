class Solution {
    public int minMutation(String start, String end, String[] bank) {
        Set<String> bankSet = new HashSet<>();
        for(int i = 0; i < bank.length; i++) {
            bankSet.add(bank[i]);
        }

        Queue<String> q = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        q.add(start);
        int num = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            num++;
            for(int i = 0; i < size; i++) {
                String pattern = q.poll();
                visited.add(pattern);

                for(String s : bankSet) {
                    if(isOneTransform(pattern, s) && !visited.contains(s)) {
                        if(s.equals(end)) {
                            return num;
                        }
                        q.add(s);
                    }
                }
            }
        }
        return -1;
    }

    private boolean isOneTransform(String s1, String s2) {
        int diff = 0;
        for(int i = 0; i < s1.length(); i++) {
            if(s1.charAt(i) != s2.charAt(i)) {
                diff++;
            }
        }

        return (diff == 1) ? true : false;
    }
}
