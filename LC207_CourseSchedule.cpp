class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        map<int, vector<int> > graph;

        for(int i = 0; i < prerequisites.size(); i++) {
            int pre = prerequisites[i].second;
            int course = prerequisites[i].first;
            if(graph.find(pre) == graph.end()) {
                graph[pre] = vector<int>(1, course);
            } else {
                graph[pre].push_back(course);
            }
        }

        set<int> visited;
        set<int> visiting;

        for(int i = 0; i < numCourses; i++) {
            if(!DFS(graph, i, visited)) {
                return false;
            }
        }
        return true;
    }

    bool DFS(map<int, vector<int> >& graph, int course, set<int>& visited) {
        //cycle found
        if(visited.find(course) != visited.end()) {
            return false;
        }
        vector<int> neighbors = graph[course];
        visited.insert(course);
        // visiting.insert(course);
        for(int i = 0;  i < neighbors.size(); i++) {
            //cycle found
            // if(visiting.find(neighbors[i]) != visiting.end()) {
            //     return false;
            // }

            if(!DFS(graph, neighbors[i], visited)) {
                return false;
            }
        }
        // visited.insert(course);
        // visiting.erase(course);
        visited.erase(course);
        return true;
    }
};
