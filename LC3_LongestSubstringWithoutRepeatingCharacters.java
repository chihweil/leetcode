class Solution {
    public int lengthOfLongestSubstring(String s) {
        int start = 0, end = 0;
        Set seen = new HashSet<>();
        int maxLen = 0;
        while(end < s.length()) {
            if(!seen.contains(s.charAt(end))) {
                seen.add(s.charAt(end));
                end++;
                maxLen = Math.max(maxLen, seen.size());
            } else {
                seen.remove(s.charAt(start));
                start++;
            }
        }
        return maxLen;
    }
}
