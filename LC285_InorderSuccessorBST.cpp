TreeNode* inorderSuccessor(TreeNode* root, TreeNode* p) {

    TreeNode* cur = root;
    TreeNode* next = NULL;

    while(cur) {
        if(p->val < cur->val) {
            next = cur;
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }

    return next;
}
