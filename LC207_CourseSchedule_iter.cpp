class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        map<int, vector<int> > graph;
        for(int i = 0; i < numCourses; i++) {
            graph[i] = vector<int>(0);
        }

        for(int i = 0; i < prerequisites.size(); i++) {
            int pre = prerequisites[i].second;
            int course = prerequisites[i].first;
            graph[pre].push_back(course);
        }

        set<int> visited;
        set<int> visiting;
        stack<int> s;
        for(int i = 0; i < numCourses; i++) {
            if(visited.find(i) != visited.end()) {
                continue;
            }
            s.push(i);
            while(!s.empty()) {
                int node = s.top();
                s.pop();
                visiting.insert(node);
                vector<int> neighbors = graph[node];
                if(neighbors.size() == 0) {
                    visiting.clear();
                }
                for(int j = 0; j < neighbors.size(); j++) {
                    if(visiting.find(neighbors[j]) != visiting.end()) {
                        return false;
                    }
                    s.push(neighbors[j]);
                }
                visited.insert(node);
            }
        }
        return true;
    }
};
