/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */


class Solution {
public:
    /*
     * @param A: an integer array
     * @return: A tree node
     */
    TreeNode * sortedArrayToBST(vector<int> &A) {
        // write your code here
        TreeNode* root = createBST(0, A.size()-1, A);
        return root;
    }

    TreeNode* createBST(int l, int r, vector<int>& nums) {
        if(l > r) {
            return NULL;
        }
        int m = (l+r)/2;
        TreeNode* node = new TreeNode(nums[m]);
        node->left = createBST(l, m-1, nums);
        node->right = createBST(m+1, r, nums);
        return node;
    }
};
