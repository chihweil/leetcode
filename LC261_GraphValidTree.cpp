class Solution {
public:
    bool validTree(int n, vector<pair<int, int>>& edges) {
        map<int, vector<int>> graph;
        for(int i = 0; i < n; i++) {
            graph[i] = vector<int>();
        }
        for(int i = 0; i < edges.size(); i++) {
            int node1 = edges[i].first;
            int node2 = edges[i].second;
            graph[node1].push_back(node2);
            graph[node2].push_back(node1);
        }

        stack<pair<int, int>> s;
        set<int> visited;
        s.push(make_pair(0, -1));
        while(!s.empty()) {
            pair<int, int> node = s.top();
            s.pop();
            visited.insert(node.first);
            vector<int> neighbors = graph[node.first];
            for(int n : neighbors) {
                if(visited.find(n) == visited.end() ) {
                    s.push(make_pair(n, node.first));
                } else if(n != node.second) {
                    return false;
                }
            }
        }
        return (visited.size() == n ) ? true : false;
    }
};
