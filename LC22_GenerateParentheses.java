class Solution {
    public List<String> generateParenthesis(int n) {
        List<String> res = new ArrayList<>();
        create(n, 0, 0, "", res);
        return res;
    }

    private void create(int n, int left, int right, String tmp, List<String> res) {
        if(tmp.length() == n*2) {
            res.add(tmp);
            return;
        }

        if(left < n) {
            create(n, left+1, right, tmp+"(", res);
        }

        if(right < left && right < n) {
            create(n, left, right+1, tmp+")", res);
        }
    }
}
