class Solution {
public:
    void solve(vector<vector<char>>& board) {
        int m = board.size();
        if(m == 0) return;
        int n = board[0].size();

        for(int i = 0; i < m; i++) {
            findBorder(m, n, i, 0, board);
            findBorder(m, n, i, n-1, board);
        }

        for(int j = 0; j < n; j++) {
            findBorder(m, n, 0, j, board);
            findBorder(m, n, m-1, j, board);
        }

        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(board[i][j] == 'O') {
                    board[i][j] = 'X';
                } else if(board[i][j] == 'B') {
                    board[i][j] = 'O';
                }
            }
        }
    }

    void findBorder(int m, int n, int i, int j, vector<vector<char>>& board) {
        if(i < 0 || i>=m || j<0 || j >= n) {
            return;
        }

        if(board[i][j] == 'O') {
            board[i][j] = 'B';
            findBorder(m, n, i+1, j, board);
            findBorder(m, n, i-1, j, board);
            findBorder(m, n, i, j+1, board);
            findBorder(m, n, i, j-1, board);
        }
    }
};
