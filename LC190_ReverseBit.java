public class ReverseBits {
    // you need treat n as an unsigned value
    public int reverseBits(int n) {
        int ans = 0;
        int x;
        for(int i = 0; i < 32; i++) {
            x = n & 1;
            n = n >> 1;
            ans = (ans << 1) | x;
        }

        return ans;
    }
}
