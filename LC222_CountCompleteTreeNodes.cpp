/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int countNodes(TreeNode* root) {
        if(root == NULL) {
            return 0;
        }

        int left = leftDepth(root);
        int right = rightDepth(root);
        if(left == right) {
            return (1 << left) - 1;
        }

        return 1 + countNodes(root->left) + countNodes(root->right);
    }

    int leftDepth(TreeNode* root) {
        int d = 0;
        while(root != NULL) {
            d++;
            root = root->left;
        }
        return d;
    }

    int rightDepth(TreeNode* root) {
        int d = 0;
        while(root != NULL) {
            d++;
            root = root->right;
        }
        return d;
    }
};
