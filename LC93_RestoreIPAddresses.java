class Solution {
    public List<String> restoreIpAddresses(String s) {
        List<String> addresses = new ArrayList<>();
        createIpAddresses(s, "", 0, addresses);
        return addresses;
    }

    private void createIpAddresses(String s, String tmp, int numOfDot, List<String> addresses) {
        if(numOfDot > 4) {
            return;
        }
        if(numOfDot == 4 && s.length() == 0) {
            addresses.add(tmp.substring(0, tmp.length()-1));
            return;
        }

        for(int i = 1; i <= 3; i++) {
            if(s.length() < i) {
                continue;
            }
            int value = Integer.parseInt(s.substring(0,i));
            if(value > 255 || i != String.valueOf(value).length()) {
                continue;
            }
            createIpAddresses(s.substring(i), tmp + s.substring(0, i) + ".", numOfDot+1, addresses);
        }
    }
}
