class Solution {
    class TrieNode {
        private TrieNode[] children = new TrieNode[26];
        private String word;
        public TrieNode() {
            word = null;
            for(int i = 0; i < 26; i++) children[i] = null;
        }
    }

    private List<String> wordList;

    public List<String> findWords(char[][] board, String[] words) {

        wordList = new ArrayList<>();
        TrieNode root = new TrieNode();
        //create word tree
        for(String str : words) {
            TrieNode tmpNode = root;
            for(char c : str.toCharArray()) {
                if(tmpNode.children[c-'a'] == null) {
                    tmpNode.children[c-'a'] = new TrieNode();
                }
                tmpNode = tmpNode.children[c-'a'];
            }
            tmpNode.word = str;
        }

        int m = board.length;
        int n = board[0].length;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                findWord(board, i, j, m, n, root);
            }
        }

        return wordList;
    }

    public void findWord(char[][] board, int i, int j, int m, int n, TrieNode root) {
        if(i < 0 || i > m-1 || j < 0 || j > n-1 ) {
            return;
        }

        char c = board[i][j];
        if(c == '*' || root.children[c-'a'] == null) {
            return;
        }
        root = root.children[c-'a'];
        if(root.word != null) {
            wordList.add(root.word);
            root.word = null; //avoid duplicate
        }

        board[i][j] = '*';
        findWord(board, i+1, j, m, n, root);
        findWord(board, i-1, j, m, n, root);
        findWord(board, i, j+1, m, n, root);
        findWord(board, i, j-1, m, n, root);
        board[i][j] = c;
    }
}
