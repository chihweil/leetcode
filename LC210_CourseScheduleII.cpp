class Solution {
public:
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
        map<int, vector<int> > graph;
        vector<int> indegree(numCourses,0);
        for(int i = 0; i < numCourses; i++) {
            graph[i] = vector<int>(0);
        }

        for(int i = 0; i < prerequisites.size(); i++) {
            int pre = prerequisites[i].second;
            int course = prerequisites[i].first;
            graph[pre].push_back(course);
            indegree[course]++;
        }

        queue<int> q;
        vector<int> res;
        for(int i = 0; i<numCourses; i++) {
            if(indegree[i] == 0) {
                q.push(i);
                res.push_back(i);
            }
        }

        while(!q.empty()) {
            int node = q.front();
            q.pop();
            vector<int> neighbors = graph[node];
            for(int i = 0; i < neighbors.size(); i++) {
                indegree[neighbors[i]]--;
                if(indegree[neighbors[i]] == 0) {
                    q.push(neighbors[i]);
                    res.push_back(neighbors[i]);
                }
            }
        }

        return (res.size() == numCourses) ? res : vector<int>(0);
    }
};
