public class RepeatedDNASequences {
    public List<String> findRepeatedDNASequences (String s) {
        Map<String, Boolean> DNASet = new HashMap<>();
        List<String> repeatedDNA = new ArrayList<>();

        for(int i = 0; i < s.length()-9; i++) {
            String pattern = s.substring(i, i+10);
            if(!DNASet.containsKey(pattern)) {
                DNASet.put(pattern, false);
            } else {
                if(!DNASet.get(pattern)) {
                    repeatedDNA.add(pattern);
                }
                DNASet.put(pattern, true);
            }
        }
        return repeatedDNA;
    }
}
