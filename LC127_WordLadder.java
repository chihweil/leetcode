class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordSet = new HashSet<>();
        for(String s : wordList) {
            wordSet.add(s);
        }

        Set<String> visited = new HashSet<>();
        Queue<String> q = new LinkedList<>();
        q.add(beginWord);
        int count = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            count++;
            for(int i = 0; i < size; i++) {
                String str = q.poll();
                visited.add(str);
                if(str.equals(endWord)) {
                    return count;
                }
                List<String> possibleWords = checkOneTransform(str, wordSet);
                for(int j = 0; j < possibleWords.size(); j++) {
                    if(!visited.contains(possibleWords.get(j))) {
                        q.add(possibleWords.get(j));
                    }
                }
            }
        }
        return 0;
    }

    private List<String> checkOneTransform(String s1, Set<String> wordSet) {
        List<String> possibleWords = new ArrayList<>();
        for(int i = 0; i < s1.length(); i++) {
            StringBuilder str = new StringBuilder(s1);
            for(int j = 0; j < 26; j++) {
                str.setCharAt(i, (char)((int)'a'+j));
                String s = str.toString();
                if(!s.equals(s1) && wordSet.contains(s)) {
                    possibleWords.add(s);
                }
            }
        }
        return possibleWords;
    }
}
