class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<string> s;
        for(int i = 0; i < tokens.size(); i++) {
            if(isOp(tokens[i])) {
                string second = s.top();
                s.pop();
                string first = s.top();
                s.pop();
                int res = calculate(stoi(first) , stoi(second), tokens[i]);
                s.push(to_string(res));
            } else {
                s.push(tokens[i]);
            }
        }
        int ans = stoi(s.top());
        return ans;
    }

private:
    bool isOp(string s) {
        return s == "+" || s == "-" || s == "*" || s== "/";
    }

    int calculate(int first, int second, string op) {
        if(op == "+") {
            return first + second;
        } else if(op == "-") {
            return first - second;
        } else if(op == "*") {
            return first * second;
        } else if(op == "/") {
             return first / second;
        }
    }
};
