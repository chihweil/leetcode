vector<vector<string>> groupAnagrams(vector<string>& strs) {
    vector<vector<string>> res;
    map<string, vector<string>> anagrams;
    anagrams.clear();
    for(string s : strs) {
        string sortedStr = s;
        sort(sortedStr.begin(), sortedStr.end());
        if(anagrams.find(sortedStr) == anagrams.end()) {
            anagrams[sortedStr] = vector<string>(1, s);
        } else {
            anagrams[sortedStr].push_back(s);
        }
    }

    for(auto it = anagrams.begin(); it != anagrams.end(); it++) {
        res.push_back(it->second);
    }

    return res;
}
