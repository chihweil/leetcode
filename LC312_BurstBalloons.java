class Solution {
    public int maxCoins(int[] nums) {
        int n = nums.length;
        List<Integer> newNums = new ArrayList<>();
        newNums.add(1);
        for(int num: nums) {
            newNums.add(num);
        }
        newNums.add(1);
        int dp[][] = new int[n+2][n+2];


        for(int l = 1; l <= n; l++) {
            for(int i = 1; i < n+2-l; i++) {
                int j = i + l - 1; // i -> start j -> end
                for(int k = i; k <= j; k++){
                    dp[i][j] = Math.max(dp[i][j], dp[i][k-1] + newNums.get(i-1)*newNums.get(k)*newNums.get(j+1) + dp[k+1][j]);
                }
            }
        }

        return dp[1][n];
    }
}
