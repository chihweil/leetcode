class Solution {
    public int findMin(int[] nums) {
        int n = nums.length;
        int left = 0;
        int right = n-1;
        int mid;

        while(left < right) {
            mid = (left + right) / 2;
            if(nums[left] < nums[right]) {
                return nums[left];
            } else {
                if (nums[mid] >= nums[left]) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            }
        }

        return nums[left];
    }
}
