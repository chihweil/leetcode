/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        if(root == NULL) {
            return true;
        }

        int left = findDepth(root->left);
        int right = findDepth(root->right);
        bool valid = true;
        if(abs(left - right) > 1) {
            valid = false;
        }
        return valid && isBalanced(root->left) && isBalanced(root->right);
    }

    int findDepth(TreeNode* root) {
        if(root == NULL) {
            return 0;
        }
        int left = findDepth(root->left);
        int right = findDepth(root->right);

        return max(left, right) + 1;
    }


};
