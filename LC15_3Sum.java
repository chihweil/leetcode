class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> threeSumResult = new ArrayList<>();
        Arrays.sort(nums);

        for(int i = 0; i < nums.length; i++) {
            if(i > 0 && nums[i] == nums[i-1]) {
                continue;
            }
            int comp = 0 - nums[i];
            int left = i+1;
            int right = nums.length - 1;
            while(left < right) {
                int sum = nums[left] + nums[right];
                if(sum == comp) {
                    List<Integer> sol = new ArrayList<>(Arrays.asList(nums[i], nums[left], nums[right]));
                    threeSumResult.add(sol);
                    int l = nums[left];
                    int r = nums[right];
                    while(nums[left] == l && left < right) {
                        left++;
                    }
                    while(nums[right] == r && left < right) {
                        right--;
                    }
                } else if(sum < comp) {
                    left++;
                } else {
                    right--;
                }
            }
        }
        return threeSumResult;
    }
}
