/**
 * Definition for Directed graph.
 * struct DirectedGraphNode {
 *     int label;
 *     vector<DirectedGraphNode *> neighbors;
 *     DirectedGraphNode(int x) : label(x) {};
 * };
 */


class Solution {
public:
    /*
     * @param graph: A list of Directed graph node
     * @param s: the starting Directed graph node
     * @param t: the terminal Directed graph node
     * @return: a boolean value
     */
    bool hasRoute(vector<DirectedGraphNode*> graph, DirectedGraphNode* s, DirectedGraphNode* t) {
        // write your code here
        if(s == t) return true;
        queue<DirectedGraphNode* > que;
        set<DirectedGraphNode* > visited;
        que.push(s);
        visited.insert(s);
        while(!que.empty()) {
            DirectedGraphNode* node = que.front();
            que.pop();
            vector<DirectedGraphNode*> neighbors = node->neighbors;
            for(auto n : neighbors) {
                if(n == t) {
                    return true;
                }

                if(visited.find(n) == visited.end()) {
                    que.push(n);
                    visited.insert(n);
                }
            }
        }

        return false;
    }
};
