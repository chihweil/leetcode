/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        if(root == NULL) {
            return;
        }
        queue<TreeLinkNode* > q;
        q.push(root);

        while(!q.empty()) {
            int size = q.size();
            TreeLinkNode* cur;
            for(int i = 0; i < size; i++) {
                TreeLinkNode* node = q.front();
                q.pop();

                if(i == 0) {
                    cur = node;
                } else {
                    cur->next = node;
                    cur = cur->next;
                }

                if(node->left) {
                    q.push(node->left);
                }

                if(node->right) {
                    q.push(node->right);
                }
            }
            cur->next = NULL;
        }
    }
};
