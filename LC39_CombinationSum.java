class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        findCombinationSum(candidates, target, 0, 0, result, new ArrayList<>());
        return result;
    }

    private void findCombinationSum(int[] candidates, int target, int sum, int start, List<List<Integer>> result, List<Integer> tmp) {
        if(sum == target) {
            result.add(new ArrayList<>(tmp));
            return;
        }

        if(sum > target) {
            return;
        }

        for(int i = start; i < candidates.length; i++) {
            tmp.add(candidates[i]);
            findCombinationSum(candidates, target, sum+candidates[i], i, result, tmp);
            tmp.remove(tmp.size()-1);
        }
    }
}
