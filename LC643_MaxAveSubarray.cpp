class Solution {
public:
    double findMaxAverage(vector<int>& nums, int k) {
        int sum = 0;
        for(int i = 0; i < k; i++) {
            sum += nums[i];
        }

        int maximum = sum;
        for(int i = 1; i+k-1 < nums.size(); i++) {
            sum = sum - nums[i-1] + nums[i+k-1];
            maximum = max(maximum, sum);
        }
        double average = (double)maximum / k;
        return average;
    }
};
